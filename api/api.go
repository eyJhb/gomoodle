package api

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/rs/zerolog/log"
)

const (
	webservicePath = "webservice/rest/server.php"
)

type Client struct {
	BaseURL  string
	ApiToken string

	userId *int
}

func (c *Client) formatURL(funcName string) string {
	// construct query
	v := url.Values{}
	v.Set("wstoken", c.ApiToken)
	v.Set("moodlewsrestformat", "json")
	v.Set("wsfunction", funcName)
	query := v.Encode()

	return fmt.Sprintf("%s/%s?%s", c.BaseURL, webservicePath, query)
}

func (c *Client) formatDLURL(link string) string {
	// construct query
	url, err := url.Parse(link)
	if err != nil {
		log.Error().Err(err)
	}

	// add token
	values := url.Query()
	values.Add("token", c.ApiToken)
	url.RawQuery = values.Encode()

	return url.String()
}

func (c *Client) DownloadFile(ctx context.Context, link, name, outPath string) error {
	newLink := c.formatDLURL(link)

	res, err := http.Get(newLink)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	// create the output
	if err := os.MkdirAll(outPath, os.ModePerm); err != nil {
		return err
	}

	// create output file
	filename := fmt.Sprintf("%s/%s", outPath, name)
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = io.Copy(f, res.Body)
	return err
}

func (c *Client) Exec(ctx context.Context, funcName string, b string) (*http.Response, error) {
	req, err := http.NewRequestWithContext(ctx, "POST", c.formatURL(funcName), strings.NewReader(b))
	//req, err := http.NewRequestWithContext(ctx, "POST", "https://httpbin.org/post", strings.NewReader(b))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	if err != nil {
		return nil, fmt.Errorf("Exec: could not make request ready %w", err)
	}

	return http.DefaultClient.Do(req)
}
