package api

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"strconv"

	md "github.com/JohannesKaufmann/html-to-markdown"
	"github.com/rs/zerolog/log"
)

type Topic struct {
	Id      int
	Section int
	Name    string
	Summary string
	Modules []Module
}

type Module struct {
	Id       int
	Name     string
	Modname  string
	Contents []Content
}

type Content struct {
	Type     string
	Filename string
	Fileurl  string

	Timecreated  int
	Timemodified int
}

func (c *Client) DownloadCourse(ctx context.Context, courseName string, courseId int) error {
	// first get our course
	topics, err := c.CoreCourseGetContents(ctx, courseId)
	if err != nil {
		return err
	}

	// init a html-to-markdown converter
	converter := md.NewConverter("", true, nil)

	// loop over the topics
	for _, topic := range topics {
		topicName := fmt.Sprintf("%d-%s", topic.Section, topic.Name)
		path := fmt.Sprintf("%s/%s", courseName, topicName)

		// ensure dir exists
		if err := os.MkdirAll(path, os.ModePerm); err != nil {
			return err
		}

		// convert topic summary to md and write it
		md, err := converter.ConvertString(topic.Summary)
		if err != nil {
			return err
		}
		if err := ioutil.WriteFile(fmt.Sprintf("%s/summary.md", path), []byte(md), os.ModePerm); err != nil {
			return err
		}

		// download files
		for _, module := range topic.Modules {
			for _, content := range module.Contents {
				log.Info().Msgf("Downloading file %s", content.Filename)
				if err := c.DownloadFile(ctx, content.Fileurl, content.Filename, path); err != nil {
					return fmt.Errorf("DownloadCourse: failed to download '%s' from course '%s' on topic '%s' with error %w", content.Filename, courseName, topicName, err)
				}
			}
		}
	}

	return nil
}

func (c *Client) CoreCourseGetContents(ctx context.Context, courseId int) ([]Topic, error) {
	fn := "core_course_get_contents"

	form := url.Values{}
	form.Add("courseid", strconv.Itoa(courseId))

	apiRes, err := c.Exec(ctx, fn, form.Encode())
	if err != nil {
		return nil, fmt.Errorf("CoreCourseGetContents: failed to exec %w", err)
	}

	// bytesRes, err := ioutil.ReadAll(apiRes.Body)
	// fmt.Println(string(bytesRes))

	// var res CoreEnrolUsersCourses
	var res []Topic
	if err := json.NewDecoder(apiRes.Body).Decode(&res); err != nil {
		return nil, fmt.Errorf("CoreCourseGetContents: failed to decode %w", err)
	}

	return res, nil
}
