package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
)

type CoreWebServiceSiteInfo struct {
	Sitename  string //site name
	Username  string //username
	Firstname string //first name
	Lastname  string //last name
	Fullname  string //user full name
	Lang      string //Current language.
	UserId    int    //user id
	SiteURL   string //site url
}

type Course struct {
	Id          int    //id of course
	Shortname   string //short name of course
	Fullname    string //long name of course
	Displayname string // Optional //course display name for lists.

	Startdate int
	Enddate   int
}

type CoreEnrolUsersCourses struct {
	Courses []Course
}

func (c *Client) CoreGetWebserviceSiteInfo(ctx context.Context) (*CoreWebServiceSiteInfo, error) {
	fn := "core_webservice_get_site_info"

	apiRes, err := c.Exec(ctx, fn, "")
	if err != nil {
		return nil, fmt.Errorf("CoreGetWebserviceSiteInfo: failed to exec %w", err)
	}

	var res CoreWebServiceSiteInfo
	if err := json.NewDecoder(apiRes.Body).Decode(&res); err != nil {
		return nil, fmt.Errorf("CoreGetWebserviceSiteInfo: failed to decode %w", err)
	}

	return &res, nil
}

// func (c *Client) CoreGetEnrolUsersCourses(ctx context.Context) (*CoreEnrolUsersCourses, error) {
func (c *Client) CoreGetEnrolUsersCourses(ctx context.Context) ([]Course, error) {
	fn := "core_enrol_get_users_courses"

	if c.userId == nil {
		siteInfo, err := c.CoreGetWebserviceSiteInfo(ctx)
		if err != nil {
			return nil, err
		}

		c.userId = &siteInfo.UserId
	}

	form := url.Values{}
	form.Add("userid", strconv.Itoa(*c.userId))

	apiRes, err := c.Exec(ctx, fn, form.Encode())
	if err != nil {
		return nil, fmt.Errorf("CoreGetEnrolUsersCourses: failed to exec %w", err)
	}

	// bytesRes, err := ioutil.ReadAll(apiRes.Body)
	// fmt.Println(string(bytesRes))

	// var res CoreEnrolUsersCourses
	var res []Course
	if err := json.NewDecoder(apiRes.Body).Decode(&res); err != nil {
		return nil, fmt.Errorf("CoreGetEnrolUsersCourses: failed to decode %w", err)
	}

	return res, nil
}
