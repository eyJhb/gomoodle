# Go Moodle!
This is a simple Moodle Scraper using the Mobile API to do it.
Simply run the program, specify the URL and your token, and you are ready to go!

The URL and token can be specified using either environment variables, configuration file or the flags.
The priority goes flag, env configuration.

```
GOMOODLE_URL="https://...." GOMOODLE_TOKEN="1234" go run main.go 
```

Or create a configuration file in `~/.config/gomoodle/config.yml`, with the content

```yaml
URL = https://
Token = 1234
```

```
Usage of gomoodle [-flags] <search term>:
  -token Moodle mobile web service
    	The API Token for Moodle mobile web service to use (Preferences->Security Keys)
  -url string
    	The URL for the moodle instance, e.g. https://godoc.org/flag
```

