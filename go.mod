module gitlab.com/eyJhb/gomoodle

go 1.15

require (
	github.com/JohannesKaufmann/html-to-markdown v1.2.0
	github.com/rs/zerolog v1.20.0
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
