package cmd

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
	"strings"

	"github.com/rs/zerolog/log"
	"gitlab.com/eyJhb/gomoodle/api"
	"gopkg.in/yaml.v3"
)

const (
	// prefix with os.userConfigDir
	defaultConfigLocation = "gomoodle/config.yml"

	// default env vars
	envUrl   = "GOMOODLE_URL"
	envToken = "GOMOODLE_TOKEN"
)

var (
	flagMoodleUrl   = flag.String("url", "", "The URL for the moodle instance, e.g. https://www.moodle.aau.dk/")
	flagMoodleToken = flag.String("token", "", "The API Token for `Moodle mobile web service` to use (Preferences->Security Keys)")
)

func priorityString(s1, s2, s3 string) string {
	if s1 != "" {
		return s1
	} else if s2 != "" {
		return s2
	}
	return s3
}

// default config
type config struct {
	URL   string `yaml:"url"`
	Token string `yaml:"token"`
}

func usage() {
	fmt.Fprintf(os.Stderr, "Usage of %s [-flags] <search term>:\n", os.Args[0])
	flag.PrintDefaults()
}

func Run() {
	// set usage and parse cmd/flags
	flag.Usage = usage
	flag.Parse()

	// try to parse config file
	conf := tryLoadConfig()

	url := priorityString(*flagMoodleUrl, os.Getenv(envUrl), conf.URL)
	token := priorityString(*flagMoodleToken, os.Getenv(envToken), conf.Token)

	// we want non-empty stuff
	if url == "" {
		fmt.Fprint(os.Stderr, "Please provide a URL (using flag, env or configuration)\n")
		usage()
		return
	}
	if token == "" {
		fmt.Fprint(os.Stderr, "Please provide a token (using flag, env or configuration)\n")
		usage()
		return
	}

	var term string
	if flag.NArg() != 1 {
		fmt.Fprintf(os.Stderr, "Please provide a search term\n")
		usage()
		return
	}
	term = strings.ToLower(flag.Arg(0))
	fmt.Fprintf(os.Stdout, "Searching for the coursenames containing '%s'\n", term)

	// init our client
	client := api.Client{
		BaseURL:  url,
		ApiToken: token,
	}

	ctx := context.TODO()

	courses, err := client.CoreGetEnrolUsersCourses(ctx)
	if err != nil {
		log.Printf("failed to get courses the user is enrolled in %v", err)
		return
	}

	// sort courses by start date
	sort.SliceStable(courses, func(i, j int) bool {
		if courses[i].Startdate > courses[j].Startdate {
			return true
		}
		return false
	})

	var counter int
	courseMap := make(map[int]*api.Course)
	for i, course := range courses {
		matchName := strings.ToLower(course.Shortname)
		if !strings.Contains(matchName, term) {
			continue
		}
		fmt.Printf("%d - %s\n", counter, course.Shortname)

		// add to our map and increase counter
		courseMap[counter] = &courses[i]
		counter++
	}

	if counter == 0 {
		fmt.Fprintln(os.Stdout, "Did not find any courses matching that name")
		return
	}

	fmt.Fprintln(os.Stdout, "Please give a comma seperated list of courses to download, e.g. 0,1,2,3,4")
	coursesString := getInput("Course List")

	// split courses
	coursesList := strings.Split(coursesString, ",")

	// check if all courses specified is in the list
	for _, course := range coursesList {
		courseIndex, err := strconv.Atoi(strings.TrimSpace(course))
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to convert id (%s) to int with error %v\n", course, err)
			return
		}

		// check if course exitsts in list
		if courseMap[courseIndex] == nil {
			fmt.Fprintf(os.Stderr, "Could not find the course specified by number %d\n", course)
			return
		}
	}

	// download them all!
	for _, course := range coursesList {
		courseIndex, err := strconv.Atoi(strings.TrimSpace(course))
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to convert id (%s) to int with error %v\n", course, err)
			return
		}
		courseMod := courseMap[courseIndex]

		if err := client.DownloadCourse(ctx, fmt.Sprintf("%d-%s", courseIndex, courseMod.Displayname), courseMod.Id); err != nil {
			fmt.Fprintf(os.Stderr, "Failed to download course %s with error %v\n", courseMod.Displayname, err)
			return
		}
	}

	fmt.Println("Finished")
}

func getInput(prompt string) string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Fprintf(os.Stdout, "%s: ", prompt)
	text, _ := reader.ReadString('\n')
	return text[:len(text)-1]
}

func tryLoadConfig() config {
	// get UserConfigDir
	confDir, err := os.UserConfigDir()
	if err != nil {
		log.Printf("tryLoadConfig: failed to get os.UserConfigDir() with error %v", err)
		return config{}
	}

	// path for config
	path := fmt.Sprintf("%s/%s", confDir, defaultConfigLocation)

	byteContent, err := ioutil.ReadFile(path)
	if err != nil {
		// log.Printf("tryLoadConfig: failed to read config file, might not exists %v", err)
		return config{}
	}

	var c config
	if err := yaml.Unmarshal(byteContent, &c); err != nil {
		log.Printf("tryLoadConfig: failed to parse configuration file %v", err)
		return config{}
	}

	return c
}
